# README #

## Get started ##
*	Copy the files from this repo into your new repo (excluding .git folder)
*	Make sure you have added your public SSH key to your bitbucket account
*	In the repo, run `yarn`. This will install all the dependencies.
*	Next, get coding :), add more Handlers and add them to index.js


## Environment Variables ##
Internal project variables


### Deployment Environment Vars ###
Used for deploying the Node code to AWS lambda function.

*	AWS_ACCESS_KEY_ID
*	AWS_SECRET_ACCESS_KEY
* 	AWS_DEFAULT_REGION
*	AWS_IAM_ROLE
*	LAMDA_FUNCTION_NAME - without -development or -production suffix
