const snippets = {
    GREETING_HELLO: {
        en: {
            text:
        'Hello there, how are you?'
        }
    }
};

module.exports = {
    snippets
};
