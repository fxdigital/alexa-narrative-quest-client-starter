const AVSL = require('@fx_digital/alexa-vui-sentry-logger');

const DefaultErrorHandler = {
    canHandle() {
        return true;
    },
    async handle(handlerInput, error) {
        const speakOutput = `Oops. There was an error. ${error.message}`;
        console.log(`~~~~ Error handled: ${JSON.stringify(error)}`);
        console.error(error);

        try {
            await AVSL.logError(error);
        } catch (e) {
            console.warn('Failed to post error to Sentry.');
        }

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    },
    getTrace() {
        return {
            handlerName: 'DefaultErrorHandler',
            alwaysKeepsSessionOpen: true,
            alwaysClosesSession: false,
            isErrorHandler: true
        };
    }
};

module.exports = {
    DefaultErrorHandler
};
