const AnyHandler = {
    canHandle() {
        return true;
    },
    handle() {
        throw new Error('The any handler was called. This should not happen.');
    },
    getTrace() {
        return {
            handlerName: 'AnyHandler'
        };
    }
};

module.exports = {
    AnyHandler
};
