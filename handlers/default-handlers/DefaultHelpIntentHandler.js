const Alexa = require('ask-sdk');
const { intents, requestTypes } = require('@skill');

const DefaultHelpIntentHandler = {
    canHandle(handlerInput) {
        return (
            Alexa.getRequestType(handlerInput.requestEnvelope)
            === requestTypes.IntentRequest
            && Alexa.getIntentName(handlerInput.requestEnvelope)
                === intents.AMAZON.HelpIntent
        );
    },
    handle(handlerInput) {
        const speak = 'Unfortunately, I can\'t help with that at the moment. What else would you like?';
        return handlerInput.responseBuilder.speak(speak).reprompt(speak).getResponse();
    },
    getTrace() {
        return {
            handlerName: 'DefaultHelpIntentHandler',
            alwaysKeepsSessionOpen: true,
            alwaysClosesSession: false
        };
    }
};

module.exports = {
    DefaultHelpIntentHandler
};
