const Alexa = require('ask-sdk');
const { RequestType } = require('@fx_digital/alexa-nq-utils');

const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return (
            Alexa.getRequestType(handlerInput.requestEnvelope)
      === RequestType.SessionEndedRequest
        );
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder.getResponse();
    },
    getTrace() {
        return {
            handlerName: 'SessionEndedRequestHandler',
            alwaysKeepsSessionOpen: false,
            alwaysClosesSession: true
        };
    }
};

module.exports = {
    SessionEndedRequestHandler
};
