const Alexa = require('ask-sdk');
const { requestTypes } = require('@skill');

const GenericLaunchRequestHandler = {
    canHandle(handlerInput) {
        return (
            Alexa.getRequestType(handlerInput.requestEnvelope)
        === requestTypes.LaunchRequest
        );
    },
    handle(handlerInput) {
        const speak = 'Welcome to the skill. What would you like?';
        return handlerInput.responseBuilder
            .speak(speak)
            .reprompt(speak)
            .getResponse();
    },
    getTrace() {
        return {
            handlerName: 'GenericLaunchRequestHandler',
            alwaysKeepsSessionOpen: true,
            alwaysClosesSession: false
        };
    }
};

module.exports = {
    GenericLaunchRequestHandler
};
