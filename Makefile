nbin = ./node_modules/.bin
prettier = $(nbin)/prettier "{,**/}{,.}*.{css,sass,scss,js,json,jsx,md,yml,yaml}"

.PHONY: clean format lint watch prepare_for_commit

clean:
	rm -rf ./node_modules

format:
	$(prettier) --write && yarn esw --fix

lint:
	$(prettier) --check

session:
	yarn run bst proxy lambda index.js

test:
	yarn esw && jest --passWithNoTests --verbose --detectOpenHandles --runInBand

prepare_for_commit: 
	$(MAKE) clean
	yarn install
	$(MAKE) format
	$(MAKE) test

watch:
	yarn esw --fix -w