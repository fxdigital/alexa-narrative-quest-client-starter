const { BuiltInIntents, RequestType } = require('@fx_digital/alexa-nq-utils');

/**
 * Library file that includes all of the features of the skill.
 */
const userStates = {
    /**
     * When the user is new to the skill.
     */
    AUDM_NEWLY_CREATED: 'AUDM_NEWLY_CREATED'
};

const sessionStates = {
    /**
     * Some state.
     */
    WELCOME: 'WELCOME'
};

const slotNames = {
    PACK: 'pack',
    STORY: 'story',
    DYNAMIC_SLOT: 'dynamicSlot'
};

const intents = {
    // Add built in intents
    ...BuiltInIntents,

    // Add the custom language model intents
    SelectWhichPackIntent: 'SelectWhichPackIntent'
};


module.exports = {
    userStates,
    sessionStates,
    slotNames,
    intents,
    requestTypes: RequestType,
    custom: {
        paidPacks: {
            EX: 'EX',
            HI: 'HI'
        }
    }
};
