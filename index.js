require('dotenv').config();
require('module-alias/register');

const AlexaSdkCore = require('ask-sdk-core');

const AVSL = require('@fx_digital/alexa-vui-sentry-logger');
// const AUDM = require('@fx_digital/alexa-user-data-manager');
const ASH = require('@fx_digital/alexa-session-helper');
const ASSH = require('@fx_digital/alexa-ssml-snippets-helper');

AVSL.setSentryInitOptions({
    dsn: process.env.SENTRY_DSN
});

/**
 * There's no need to use the AUDM package if it's not needed tbh.
 */
// AUDM.setConfig({
//   accessKeyId: process.env.AWS_DYN_ACCESS_KEY_ID,
//   secretAccessKey: process.env.AWS_DYN_SECRET_KEY,
//   region: process.env.AWS_DYN_REGION,
//   tableName: process.env.AWS_DYNDB_TABLE_NAME,
// });

/**
 * Include request handlers.
 */
const { SessionEndedRequestHandler } = require('./handlers/default-handlers/SessionEndedRequestHandler');
const { DefaultHelpIntentHandler } = require('./handlers/default-handlers/DefaultHelpIntentHandler');
const { AnyHandler } = require('./handlers/default-handlers/AnyHandler');

// Custom Skill request handlers.
const { GenericLaunchRequestHandler } = require('./handlers/LaunchRequest/LaunchRequestHandlers');

/**
 * Construct an exportable object containing all the handlers used in the skill.
 * Exporting them like this will help with testing later.
 */
const allRequestHandlers = {
    GenericLaunchRequestHandler,
    SessionEndedRequestHandler,
    DefaultHelpIntentHandler,
    AnyHandler // keep this one last.
};
exports.allRequestHandlers = allRequestHandlers;

const { DefaultErrorHandler } = require('./handlers/default-handlers/DefaultErrorHandler');
/**
 * Construct an exportable object containing all the error handlers.
 * Exporting them like this will help with testing later.
 * Note: Response interceptors are not processed after an Error handler is processed.
 * Important: To preserve data, you may need to nqUser.commit() in your error handler
 * if the handler closes the session.
 */
const allErrorHandlers = {
    DefaultErrorHandler
};
exports.allErrorHandlers = allErrorHandlers;


/**
 * Include snippets from ssml-snippets folder
 * And create an object which houses them all.
 */
const GreetingSnippets = require('./ssml-snippets/greetings');

const snippets = {
    ...GreetingSnippets.snippets
};
const getSnippets = () => snippets;
exports.getSnippets = getSnippets;


/**
 * This handler acts as the entry point for your skill, routing all request and response
 * payloads to the handlers above. Make sure any new handlers or interceptors you've
 * defined are included below. The order matters - they're processed top to bottom
 * */
exports.handler = AlexaSdkCore.SkillBuilders.custom()
    .addRequestHandlers(
        allRequestHandlers.GenericLaunchRequestHandler,
        allRequestHandlers.SessionEndedRequestHandler,
        allRequestHandlers.DefaultHelpIntentHandler,
        allRequestHandlers.AnyHandler
    )
    .addErrorHandlers(
        allErrorHandlers.DefaultErrorHandler
    )
    .withCustomUserAgent(
        `fx-digital/narrative-quest ${process.env.CUSTOM_USER_AGENT}`
    )
    .addRequestInterceptors(
        ASH.getRequestInterceptor(),

        // Enable this if you need it
        // AUDM.askRequestInterceptor,
        AVSL.askRequestInterceptor
    )

    .addResponseInterceptors(
    /**
     * Configure the snippets response interceptor
     */
        ASSH.getSSMLSnippetsResponseInterceptor({
            internalPrefix: process.env.S3_ASSETS_BASE_URL,
            snippets: () => getSnippets
        }),

        // AUDM.askResponseInterceptor,
        AVSL.askResponseInterceptor
    )

    .lambda();
